import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

// import { useStyles } from '../../../styles';
import Grid from '@material-ui/core/Grid';

import Modal from "../../../Utils/modal";
import axios from "axios";

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';

import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import Description from '@material-ui/icons/Description';
import Error from '@material-ui/icons/Error';
import TextField from '@material-ui/core/TextField';


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={event => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const useStyles_tabs = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    root2: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
        background: "#4CAF50"
    },
    center: {
        justifyContent: 'center'
    }
}));

export default function ModificarPaciente(props) {

    const classes = useStyles();

    const [value, setValue] = React.useState(0);

    // const [menuItemUsuarios, setMenuItemUsuarios] = useState([]);
    const [rutPacienteOriginal, setRutPacienteOriginal] = useState("");
    const [rutPaciente, setRutPaciente] = useState("");
    const [strNombre, setStrNombre] = useState("");
    const [strApellido, setStrApellido] = useState("");
    const [strDireccion, setStrDireccion] = useState("");
    const [strSexo, setStrSexo] = useState("Sin sexo");
    const [intEdad, setIntEdad] = useState(0);

    // Urgencias
    const [strProblema, setStrProblema] = useState("");

    // Recetas médicas
    const [strMedicamento, setStrMedicamento] = useState("");

    useEffect(() => {

        if (props.datosPaciente != "undefined") {
            console.log(props.datosPaciente);
            setRutPacienteOriginal(props.datosPaciente.rutUsuario);
            setRutPaciente(props.datosPaciente.rutUsuario);

            setStrNombre(props.datosPaciente.strNombreUsuario);
            setStrApellido(props.datosPaciente.strApellidoUsuario);
            setStrDireccion(props.datosPaciente.strDireccionUsuario);
            setStrSexo(props.datosPaciente.strSexoUsuario);
            setIntEdad(props.datosPaciente.intEdadUsuario);
            setStrProblema(props.datosPaciente.strProblema);
            setStrMedicamento(props.datosPaciente.strMedicamento);
        }

    }, [props.datosPaciente]);

    useEffect(() => {

        // axios.get(process.env.REACT_APP_SERVER+"/usuarios/all")
        //     .then((response) => {

        //         var usuarios = response.data.map((item, key) => {
        //             return (
        //                 <MenuItem value={item.rutUsuario}>{item.strNombreUsuario} {item.strApellidoUsuario}</MenuItem>
        //             );
        //         });

        //         setMenuItemUsuarios(usuarios);

        //     });
    }, [props.recargarPacientes]);

    function handleChangeStrNombre(event) {
        setStrNombre(event.target.value);
    }

    function handleChangeStrApellido(event) {
        setStrApellido(event.target.value);
    }

    function handleChangeStrDireccion(event) {
        setStrDireccion(event.target.value)
    }

    function handleChangeStrSexo(event) {
        setStrSexo(event.target.value)
    }

    function handleChangeIntEdad(event) {
        setIntEdad(event.target.value)
    }

    // Urgencias
    function handleChangeStrProblema(event) {
        setStrProblema(event.target.value)
    }

    // BOLETAS médicas
    function handleChangeStrMedicamento(event) {
        setStrMedicamento(event.target.value)
    }

    function handleSubmitPaciente() {


        var data = {
            rutPacienteOriginal,
            rutPaciente,
            strNombre,
            strApellido,
            strDireccion,
            strSexo,
            intEdad,
            strProblema,
            strMedicamento
        };

        axios.post(process.env.REACT_APP_SERVER+"/pacientes/modificar", JSON.stringify(data))
            .then((response) => {

                console.log(response);
                if (response.data.jsonResponse == "paciente_modificado") {

                    props.setVariant("success");
                    props.setMessage("Paciente modificado");
                    props.setOpen(true);

                    props.setRecargarPacientes(!props.recargarPacientes);
                    props.setOpenModificarPaciente(false);

                } else if (response.data.jsonResponse == "error_modificar_paciente") {

                    props.setVariant("error");
                    props.setMessage("A ocurrido un error, reintente");
                    props.setOpen(true);

                }

            });

    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div>
            <Modal
                classes={classes}
                open={props.openModificarPaciente}
                setOpen={props.setOpenModificarPaciente}
                title={"Modificar paciente"}
                content={


                    <div>
                        <Paper square className={classes.root2}>
                            <Tabs
                                variant="fullWidth"
                                value={value}
                                onChange={handleChange}
                                aria-label="nav tabs example"
                                textColor="primary"
                            >
                                <Tab icon={<PersonPinIcon />} label="DATOS PERSONALES" />
                                <Tab icon={<Error />} label="URGENCIAS" />
                                <Tab icon={<Description />} label="RECETAS MÉDICAS" />
                            </Tabs>
                        </Paper>
                        <TabPanel value={value} index={0}>

                            <Grid container spacing={3}>

                                <Grid item md={6} sm={12}>

                                    {/* Lista de usuarios */}
                                    <FormControl className={classes.formControl} fullWidth>

                                        <TextField
                                            id="standard-name"
                                            label="Nombre"
                                            className={classes.textField}
                                            value={strNombre}
                                            onChange={handleChangeStrNombre}
                                            margin="normal"
                                        />

                                    </FormControl>

                                </Grid>

                                <Grid item md={6} sm={12}>

                                    {/* Lista de usuarios */}
                                    <FormControl className={classes.formControl} fullWidth>

                                        <TextField
                                            id="standard-name"
                                            label="Apellido"
                                            className={classes.textField}
                                            value={strApellido}
                                            onChange={handleChangeStrApellido}
                                            margin="normal"
                                        />

                                    </FormControl>

                                </Grid>

                                <Grid item md={12} sm={12}>

                                    {/* Lista de usuarios */}
                                    <FormControl className={classes.formControl} fullWidth>

                                        <TextField
                                            id="standard-name"
                                            label="Direccion"
                                            className={classes.textField}
                                            value={strDireccion}
                                            onChange={handleChangeStrDireccion}
                                            margin="normal"
                                        />

                                    </FormControl>

                                </Grid>

                                <Grid item md={6} sm={12}>

                                    {/* Lista de usuarios */}
                                    <FormControl className={classes.formControl} fullWidth>

                                        <TextField
                                            id="standard-name"
                                            label="Sexo"
                                            className={classes.textField}
                                            value={strSexo}
                                            onChange={handleChangeStrSexo}
                                            margin="normal"
                                        />

                                    </FormControl>

                                </Grid>

                                <Grid item md={6} sm={12}>

                                    {/* Lista de usuarios */}
                                    <FormControl className={classes.formControl} fullWidth>

                                        <TextField
                                            id="standard-name"
                                            label="Edad"
                                            className={classes.textField}
                                            value={intEdad}
                                            onChange={handleChangeIntEdad}
                                            margin="normal"
                                        />

                                    </FormControl>

                                </Grid>

                            </Grid>

                        </TabPanel>
                        <TabPanel value={value} index={1}>

                            <Grid container spacing={3}>

                                <Grid item md={12} sm={12}>

                                    {/* Lista de usuarios */}
                                    <FormControl className={classes.formControl} fullWidth>

                                        <TextField
                                            id="standard-name"
                                            label="Descripción Problema"
                                            multiline
                                            className={classes.textField}
                                            value={strProblema}
                                            onChange={handleChangeStrProblema}
                                            margin="normal"
                                        />

                                    </FormControl>

                                </Grid>

                            </Grid>

                        </TabPanel>
                        <TabPanel value={value} index={2}>

                            <Grid container spacing={3}>

                                <Grid item md={12} sm={12}>

                                    {/* Lista de usuarios */}
                                    <FormControl className={classes.formControl} fullWidth>

                                        <TextField
                                            id="standard-name"
                                            label="Medicamento"
                                            className={classes.textField}
                                            value={strMedicamento}
                                            onChange={handleChangeStrMedicamento}
                                            margin="normal"
                                        />

                                    </FormControl>

                                </Grid>

                            </Grid>

                        </TabPanel>
                    </div>
                }
                actions={
                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        onClick={handleSubmitPaciente}>
                        Modificar paciente
                    </Button>
                }
            />

        </div>
    )

}

