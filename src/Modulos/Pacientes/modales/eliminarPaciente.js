import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

// import { useStyles } from '../../../styles';
import Grid from '@material-ui/core/Grid';

import Modal from "../../../Utils/modal";
import axios from "axios";

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
        background: "#4CAF50"
    },
    center: {
        justifyContent: 'center'
    }
}));

export default function EliminarPaciente(props) {

    const classes = useStyles();

    const [menuItemUsuarios, setMenuItemUsuarios] = useState([]);
    const [rutPaciente, setRutPaciente] = useState("");

    useEffect(() => {
        
        if(props.datosPaciente != "undefined") {
            setRutPaciente(props.datosPaciente.rutUsuario);
        }

    }, [props.datosPaciente]);


    function handleSubmitPaciente() {

        
        var data = {
            rutPaciente
        };

        axios.post(process.env.REACT_APP_SERVER+"/pacientes/eliminar", JSON.stringify(data))
            .then((response) => {

                console.log(response);
                if(response.data.jsonResponse == "paciente_eliminado") {

                    props.setVariant("success");
                    props.setMessage("Paciente eliminado");
                    props.setOpen(true);

                    props.setRecargarPacientes(!props.recargarPacientes);
                    props.setOpenEliminarPaciente(false);

                }else if(response.data.jsonResponse == "error_eliminar_paciente") {

                    props.setVariant("error");
                    props.setMessage("A ocurrido un error, reintente");
                    props.setOpen(true);

                }

            });

    }

    return (
        <div>
            <Modal
                classes={classes}
                open={props.openEliminarPaciente}
                setOpen={props.setOpenEliminarPaciente}
                title={"Eliminar paciente"}
                content={

                    <Grid container spacing={3} className={classes.center}>
                        <Grid item md={12}>

                            <Typography>
                                ¿Deseas eliminar a este paciente?
                            </Typography>

                        </Grid>

                    </Grid>
                }
                actions={
                    <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.button}
                        onClick={handleSubmitPaciente}>
                        Eliminar paciente
                    </Button>
                }
            />
            
        </div>
    )

}

