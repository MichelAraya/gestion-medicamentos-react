import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

// import { useStyles } from '../../../styles';
import Grid from '@material-ui/core/Grid';

import Modal from "../../../Utils/modal";
import axios from "axios";

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
        background: "#4CAF50"
    },
    center: {
        justifyContent: 'center'
    }
}));

export default function AgregarMedico(props) {

    const classes = useStyles();

    const [menuItemUsuarios, setMenuItemUsuarios] = useState([]);
    const [rutUsuario, setRutUsuario] = useState("");
    const [digitoVerificador, setDigitoVerificador] = useState("");
    const [nombre, setNombre] = useState("");
    const [apellidoPaterno, setApellidoPaterno] = useState("");
    const [apellidoMaterno, setApellidoMaterno] = useState("");
    const [cargo, setCargo] = useState("");
    const [especialidad, setEspecialidad] = useState("");


    useEffect(() => {
        
        axios.get(process.env.REACT_APP_SERVER+"/usuarios/no/pacientes")
            .then((response) => {

                var usuarios = response.data.map((item, key) => {
                    return (
                        <MenuItem value={item.rutUsuario}>{item.strNombreUsuario}</MenuItem>
                    );
                });

                setMenuItemUsuarios(usuarios);

            });

    }, [props.recargarPacientes]);


    useEffect(() => {
        console.log(props.openAgregarMedico);
    });

    function handleChangeRutUsuario(event) {
        setRutUsuario(event.target.value);
    }

    function handleChangeDigitoVerificador(event) {
        setDigitoVerificador(event.target.value);
    }

    function handleChangeNombre(event) {
        setNombre(event.target.value);
    }

    function handleChangeApellidoPaterno(event) {
        setApellidoPaterno(event.target.value);
    }

    function handleChangeApellidoMaterno(event) {
        setApellidoMaterno(event.target.value);
    }

    function handleChangeCargo(event) {
        setCargo(event.target.value);
    }

    function handleChangeEspecialidad(event) {
        setEspecialidad(event.target.value);
    }
    function handleSubmitPaciente() {

        
        var data = {
            rutUsuario
        };

        axios.post(process.env.REACT_APP_SERVER+"/pacientes/agregar", JSON.stringify(data))
            .then((response) => {

                console.log(response);
                if(response.data.jsonResponse == "paciente_creado") {

                    props.setVariant("success");
                    props.setMessage("Paciente creado");
                    props.setOpen(true);

                    props.setRecargarPacientes(!props.recargarPacientes);

                }else if(response.data.jsonResponse == "error_agregar_paciente") {

                    props.setVariant("error");
                    props.setMessage("A ocurrido un error, reintente");
                    props.setOpen(true);

                }

            });

    }

    return (
        <div>
            <Modal
                classes={classes}
                open={props.openAgregarMedico}
                setOpen={props.setOpenAgregarMedico}
                title={"Agregar Medico"}
                content={

                    <Grid container spacing={3}>

                        <Grid item md={6}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    id="standard-name"
                                    label="Rut"
                                    className={classes.textField}
                                    value={rutUsuario}
                                    onChange={handleChangeRutUsuario}
                                    margin="normal"
                                />
                            </FormControl>

                        </Grid>

                        <Grid item md={6}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    id="standard-name"
                                    label="Digito Verificador"
                                    className={classes.textField}
                                    value={digitoVerificador}
                                    onChange={handleChangeDigitoVerificador}
                                    margin="normal"
                                />
                            </FormControl>

                        </Grid>

                        <Grid item md={6}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    id="standard-name"
                                    label="Nombre"
                                    className={classes.textField}
                                    value={nombre}
                                    onChange={handleChangeNombre}
                                    margin="normal"
                                />
                            </FormControl>

                        </Grid>

                        <Grid item md={3}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    id="standard-name"
                                    label="Apellido Paterno"
                                    className={classes.textField}
                                    value={apellidoPaterno}
                                    onChange={handleChangeApellidoPaterno}
                                    margin="normal"
                                />
                            </FormControl>

                        </Grid>

                        <Grid item md={3}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    id="standard-name"
                                    label="Apellido Materno"
                                    className={classes.textField}
                                    value={apellidoMaterno}
                                    onChange={handleChangeApellidoMaterno}
                                    margin="normal"
                                />
                            </FormControl>

                        </Grid>

                        <Grid item md={3}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    id="standard-name"
                                    label="Cargo"
                                    className={classes.textField}
                                    value={cargo}
                                    onChange={handleChangeCargo}
                                    margin="normal"
                                />
                            </FormControl>

                        </Grid>

                        <Grid item md={3}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    id="standard-name"
                                    label="Especialidad"
                                    className={classes.textField}
                                    value={especialidad}
                                    onChange={handleChangeEspecialidad}
                                    margin="normal"
                                />
                            </FormControl>

                        </Grid>

                    </Grid>
                }
                actions={
                    <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.button}
                        onClick={handleSubmitPaciente}>
                        Agregar médico
                    </Button>
                }
            />
            
        </div>
    )

}

