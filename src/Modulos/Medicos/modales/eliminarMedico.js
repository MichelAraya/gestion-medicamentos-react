import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

// import { useStyles } from '../../../styles';
import Grid from '@material-ui/core/Grid';

import Modal from "../../../Utils/modal";
import axios from "axios";

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
        background: "#4CAF50"
    },
    center: {
        justifyContent: 'center'
    }
}));

export default function EliminarMedico(props) {

    const classes = useStyles();

    const [menuItemUsuarios, setMenuItemUsuarios] = useState([]);
    const [rutMedico, setRutMedico] = useState("");

    useEffect(() => {
        
        if(props.datosMedico != "undefined") {
            setRutMedico(props.datosMedico.rutUsuario);
        }

    }, [props.datosMedico]);


    function handleSubmitMedico() {

        
        var data = {
            rutMedico
        };

        axios.post(process.env.REACT_APP_SERVER+"/medicos/eliminar", JSON.stringify(data))
            .then((response) => {

                console.log(response);
                if(response.data.jsonResponse == "medico_eliminado") {

                    props.setVariant("success");
                    props.setMessage("Medico eliminado");
                    props.setOpen(true);

                    props.setRecargarMedicos(!props.recargarMedicos);
                    props.setOpenEliminarMedico(false);

                }else if(response.data.jsonResponse == "error_eliminar_medico") {

                    props.setVariant("error");
                    props.setMessage("A ocurrido un error, reintente");
                    props.setOpen(true);

                }

            });

    }

    return (
        <div>
            <Modal
                classes={classes}
                open={props.openEliminarMedico}
                setOpen={props.setOpenEliminarMedico}
                title={"Eliminar medico"}
                content={

                    <Grid container spacing={3} className={classes.center}>
                        <Grid item md={12}>

                            <Typography>
                                ¿Deseas eliminar a este Medico?
                            </Typography>

                        </Grid>

                    </Grid>
                }
                actions={
                    <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.button}
                        onClick={handleSubmitMedico}>
                        Eliminar medico
                    </Button>
                }
            />
            
        </div>
    )

}

