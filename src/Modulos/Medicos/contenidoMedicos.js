import React, { useState, useEffect } from 'react';
import { Paper } from '@material-ui/core';


import MaterialTable from 'material-table';
import axios from "axios";
import AgregarMedico from "./modales/agregarMedico";
import ModificarMedico from "./modales/modificarMedico";
import EliminarMedico from "./modales/eliminarMedico";

export default function ContenidoMedicos(props) {

    const [jsonMedicos, setJsonMedicos] = useState([]);

    const [openAgregarMedico, setOpenAgregarMedico] = useState(false);

    const [datosMedico, setDatosMedico] = useState({});
    const [openModificarMedico, setOpenModificarMedico] = useState(false);
    const [openEliminarMedico, setOpenEliminarMedico] = useState(false);

    const [recargarMedicos, setRecargarMedicos] = useState(false);

    useEffect(() => {

        axios.get(process.env.REACT_APP_SERVER+"/medicos/all")
            .then((response) => {

                setJsonMedicos([]);
                setJsonMedicos(JSON.stringify(response.data));

            })
            .catch((error) => {
                console.log(error);
            });

    }, [recargarMedicos]);


    //Colocar Apellido Paterno y Apellido Materno, sacar direccion
    const [state, setState] = React.useState({
        columns: [
            { title: 'Rut', field: 'rutUsuario' },
            { title: 'Nombre', field: 'strNombreUsuario' },
            { title: 'Apellido', field: 'strApellidoUsuario' },
            { title: 'Dirección', field: 'strDireccionUsuario' }
        ]
    });

    useEffect(() => {
        setState({});
    }, [jsonMedicos]);

    if (jsonMedicos.length == 0) {
        return (
            <div>
                Cargando médicos
            </div>
        );
    } else {
        return (
            <div>

                <AgregarMedico
                    setVariant={props.setVariant} 
                    setMessage={props.setMessage}
                    setOpen={props.setOpen}
                    openAgregarMedico={openAgregarMedico}
                    setOpenAgregarMedico={setOpenAgregarMedico}
                    recargarMedicos={recargarMedicos}
                    setRecargarMedicos={setRecargarMedicos}/>

                <ModificarMedico
                    setVariant={props.setVariant} 
                    setMessage={props.setMessage}
                    setOpen={props.setOpen}
                    openModificarMedico={openModificarMedico}
                    setOpenModificarMedico={setOpenModificarMedico}
                    recargarMedicos={recargarMedicos}
                    setRecargarMedicos={setRecargarMedicos}
                    datosMedico={datosMedico}/>

                <EliminarMedico
                    setVariant={props.setVariant} 
                    setMessage={props.setMessage}
                    setOpen={props.setOpen}
                    openEliminarMedico={openEliminarMedico}
                    setOpenEliminarMedico={setOpenEliminarMedico}
                    recargarMedicos={recargarMedicos}
                    setRecargarMedicos={setRecargarMedicos}
                    datosMedico={datosMedico}/>

                <MaterialTable
                    title="Médicos"
                    columns={[
                        { title: 'Rut', field: 'rutUsuario' },
                        { title: 'Nombre', field: 'strNombreUsuario' },
                        { title: 'Apellido', field: 'strApellidoUsuario' },
                        { title: 'Cargo', field: 'strCargo' },
                        { title: 'Especialidad', field: 'strEspecialidad' },
                    ]}
                    data={query => {
                        return new Promise((resolve) => {
                            axios.get(process.env.REACT_APP_SERVER+"/medicos/all")
                                .then((response) => {
                                    
                                    resolve({
                                        data: response.data,
                                        page: query.page,
                                        totalCount: query.totalCount
                                    });

                                })
                                .catch((error) => {
                                    console.log(error);
                                });
                        }
                        )
                    }}
                    actions={[
                        {
                            icon: 'add',
                            isFreeAction: true,
                            tooltip: 'Agregar medico',
                            onClick: (event, rowData) => {
                                setOpenAgregarMedico(true);
                            }
                        },
                        {
                            icon: 'edit',
                            tooltip: 'Editar medico',
                            onClick: (event, rowData) => {
                                setOpenModificarMedico(true);
                                setDatosMedico(rowData);
                            }
                        },
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Eliminar usuario',
                            onClick: (event, rowData) => {
                                setOpenEliminarMedico(true);
                                setDatosMedico(rowData);
                            }
                        })
                    ]}
                />
            </div>
        )
    }
}

