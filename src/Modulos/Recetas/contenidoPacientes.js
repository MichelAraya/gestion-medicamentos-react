import React, { useState, useEffect } from 'react';
import { Paper } from '@material-ui/core';
// import BotoneraTareas from './botoneraTareas';
// import TreeGridTareas from './treeGridTareas';
// import ModificarTarea from "./modificarTarea";
// import EliminarTarea from "./eliminarTarea";
// import AgregarSubTarea from "./agregarSubTarea";
// import { useStyles } from '../../../styles';

import MaterialTable from 'material-table';
import axios from "axios";
import AgregarPaciente from "./modales/agregarPaciente";
import ModificarPaciente from "./modales/modificarPaciente";
import EliminarPaciente from "./modales/eliminarPaciente";

export default function ContenidoPacientes(props) {

    const [jsonPacientes, setJsonPacientes] = useState([]);

    const [openAgregarPaciente, setOpenAgregarPaciente] = useState(false);

    const [datosPaciente, setDatosPaciente] = useState({});
    const [openModificarPaciente, setOpenModificarPaciente] = useState(false);
    const [openEliminarPaciente, setOpenEliminarPaciente] = useState(false);

    const [recargarPacientes, setRecargarPacientes] = useState(false);

    useEffect(() => {

        axios.get(process.env.REACT_APP_SERVER+"/pacientes/all")
            .then((response) => {

                setJsonPacientes([]);
                setJsonPacientes(JSON.stringify(response.data));

            })
            .catch((error) => {
                console.log(error);
            });

    }, [recargarPacientes]);

    const [state, setState] = React.useState({
        columns: [
            { title: 'Rut', field: 'rutUsuario' },
            { title: 'Nombre', field: 'strNombreUsuario' },
            { title: 'Apellido', field: 'strApellidoUsuario' },
            { title: 'Dirección', field: 'strDireccionUsuario' }
        ]
    });

    useEffect(() => {
        setState({});
    }, [jsonPacientes]);

    if (jsonPacientes.length == 0) {
        return (
            <div>
                Cargando recetas
            </div>
        );
    } else {
        return (
            <div>

                <AgregarPaciente
                    setVariant={props.setVariant} 
                    setMessage={props.setMessage}
                    setOpen={props.setOpen}
                    openAgregarPaciente={openAgregarPaciente}
                    setOpenAgregarPaciente={setOpenAgregarPaciente}
                    recargarPacientes={recargarPacientes}
                    setRecargarPacientes={setRecargarPacientes}/>

                <ModificarPaciente
                    setVariant={props.setVariant} 
                    setMessage={props.setMessage}
                    setOpen={props.setOpen}
                    openModificarPaciente={openModificarPaciente}
                    setOpenModificarPaciente={setOpenModificarPaciente}
                    recargarPacientes={recargarPacientes}
                    setRecargarPacientes={setRecargarPacientes}
                    datosPaciente={datosPaciente}/>

                <EliminarPaciente
                    setVariant={props.setVariant} 
                    setMessage={props.setMessage}
                    setOpen={props.setOpen}
                    openEliminarPaciente={openEliminarPaciente}
                    setOpenEliminarPaciente={setOpenEliminarPaciente}
                    recargarPacientes={recargarPacientes}
                    setRecargarPacientes={setRecargarPacientes}
                    datosPaciente={datosPaciente}/>

                <MaterialTable
                    title="Recetas"
                    columns={[
                        { title: 'Rut', field: 'rutUsuario' },
                        { title: 'Nombre', field: 'strNombreUsuario' },
                        { title: 'Apellido', field: 'strApellidoUsuario' },
                        { title: 'Dirección', field: 'strDireccionUsuario' }
                    ]}
                    data={query => {
                        return new Promise((resolve) => {
                            axios.get(process.env.REACT_APP_SERVER+"/pacientes/all")
                                .then((response) => {
                                    
                                    resolve({
                                        data: response.data,
                                        page: query.page,
                                        totalCount: query.totalCount
                                    });

                                })
                                .catch((error) => {
                                    console.log(error);
                                });
                        }
                        )
                    }}
                    actions={[
                        {
                            icon: 'add',
                            isFreeAction: true,
                            tooltip: 'Agregar paciente',
                            onClick: (event, rowData) => {
                                setOpenAgregarPaciente(true);
                            }
                        },
                        {
                            icon: 'edit',
                            tooltip: 'Editar paciente',
                            onClick: (event, rowData) => {
                                setOpenModificarPaciente(true);
                                setDatosPaciente(rowData);
                            }
                        },
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Eliminar usuario',
                            onClick: (event, rowData) => {
                                setOpenEliminarPaciente(true);
                                setDatosPaciente(rowData);
                            }
                        })
                    ]}
                />
            </div>
        )
    }
}

