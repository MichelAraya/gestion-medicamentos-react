import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBarTemplate from '../../Template/AppBar';
import { useStyles } from '../../styles';

// Snackbar
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import clsx from 'clsx';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';
import { amber, green } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import Bar from "./Bar";
import Container from "./Container";

import ContenidoPacientes from './contenidoPacientes';

import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

// Snackbar
const useStyles1 = makeStyles(theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.main,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
  root: {
    flexGrow: 1,
  },
}));

// Snackbar
function MySnackbarContentWrapper(props) {
  const classes = useStyles1();
  const { className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={clsx(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

export default function Pacientes() {

  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [variant, setVariant] = useState("");
  const [message, setMessage] = useState("");

  function handleClose() {
    setOpen(false);
  }

  return (
    <div className={classes.root}>

      {/* Barra superior y menú drawer */}
      <AppBarTemplate pageTitle="Tareas" />
      <main className={classes.content}>
        <div className={classes.toolbar} />

        {/* Notificaciones */}
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          open={open}
          autoHideDuration={3000}
          onClose={handleClose}
        >  
          <MySnackbarContentWrapper
            onClose={handleClose}
            variant={variant}
            message={message}
          />

        </Snackbar>

        {/*  Contenido componente tareas  */}
        <ContenidoPacientes
          setVariant={setVariant} 
          setMessage={setMessage}
          setOpen={setOpen} 
          classes={classes}/>

        

      </main>
    </div>
  );
}

