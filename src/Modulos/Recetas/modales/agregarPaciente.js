import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

// import { useStyles } from '../../../styles';
import Grid from '@material-ui/core/Grid';

import Modal from "../../../Utils/modal";
import axios from "axios";

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
        background: "#4CAF50"
    },
    center: {
        justifyContent: 'center'
    }
}));

export default function AgregarPaciente(props) {

    const classes = useStyles();

    const [menuItemUsuarios, setMenuItemUsuarios] = useState([]);
    const [rutPaciente, setRutPaciente] = useState("");

    useEffect(() => {
        
        axios.get(process.env.REACT_APP_SERVER+"/usuarios/no/pacientes")
            .then((response) => {

                var usuarios = response.data.map((item, key) => {
                    return (
                        <MenuItem value={item.rutUsuario}>{item.strNombreUsuario}</MenuItem>
                    );
                });

                setMenuItemUsuarios(usuarios);

            });
    }, [props.recargarPacientes]);


    useEffect(() => {
        console.log(props.openAgregarPaciente);
    });

    function handleChangeRutPaciente(event) {
        setRutPaciente(event.target.value);
    }

    function handleSubmitPaciente() {

        
        var data = {
            rutPaciente
        };

        axios.post(process.env.REACT_APP_SERVER+"/pacientes/agregar", JSON.stringify(data))
            .then((response) => {

                console.log(response);
                if(response.data.jsonResponse == "paciente_creado") {

                    props.setVariant("success");
                    props.setMessage("Paciente creado");
                    props.setOpen(true);

                    props.setRecargarPacientes(!props.recargarPacientes);

                }else if(response.data.jsonResponse == "error_agregar_paciente") {

                    props.setVariant("error");
                    props.setMessage("A ocurrido un error, reintente");
                    props.setOpen(true);

                }

            });

    }

    return (
        <div>
            <Modal
                classes={classes}
                open={props.openAgregarPaciente}
                setOpen={props.setOpenAgregarPaciente}
                title={"Agregar paciente"}
                content={

                    <Grid container spacing={3}>
                        <Grid item md={12}>

                            {/* Lista de usuarios */}
                            <FormControl className={classes.formControl} fullWidth>
                                <InputLabel htmlFor="age-simple">Paciente</InputLabel>
                                <Select
                                    value={rutPaciente}
                                    onChange={handleChangeRutPaciente}
                                    inputProps={{
                                        name: 'age',
                                        id: 'age-simple',
                                    }}
                                >
                                    {menuItemUsuarios}
                                </Select>
                            </FormControl>

                        </Grid>

                    </Grid>
                }
                actions={
                    <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.button}
                        onClick={handleSubmitPaciente}>
                        Agregar paciente
                    </Button>
                }
            />
            
        </div>
    )

}

