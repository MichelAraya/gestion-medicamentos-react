import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import { Axios } from '../Utils/Utils';

// import Link from '@material-ui/core/Link';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { useStyles } from '../styles';

export default function AppBarTemplate(props) {

    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const [arrayModulos, setArrayModulos] = useState([]);
    

    function handleDrawerOpen() {
        setOpen(true);
    }

    function handleDrawerClose() {
        setOpen(false);
    }

    // Cargamos los módulos cuando se renderice la vista
    useEffect(() => {

        console.log(process.env);
        // Definimos la data que será enviada
        var data = {};
        data.url = process.env.REACT_APP_SERVER+"/modulos_usuarios/por/perfil";

        // Enviamos la información y seteamos los módulos
        Axios(data).then((response) => {
            setArrayModulos(response.data);
        })
        .catch((error) => {
            console.log(error);
        });
        
    }, []);

    useEffect(() => {
        console.log(arrayModulos);
    }, [arrayModulos])

    return (
        <div>

            {/* App bar */}
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        {props.pageTitle}
                    </Typography>
                </Toolbar>
            </AppBar>

            {/* Drawer */}
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
                open={open}
            >
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </div>
                <Divider />
                <List>
                    {arrayModulos.map((item, key) => (
                        <ListItem button key={key} component={Link} to={item.strModulo}>
                            {
                                !open ? (
                                    <Tooltip title={item.strTextoModulo} placement="right-end">
                                        <ListItemIcon>
                                            <Icon>{item.strIconoModulo}</Icon>
                                        </ListItemIcon>
                                    </Tooltip>
                                ) : (
                                    <ListItemIcon>
                                        <Icon>{item.strIconoModulo}</Icon>
                                    </ListItemIcon>
                                )
                            }
                            
                            <ListItemText primary={item.strTextoModulo} />
                        </ListItem>
                    ))}
                </List>
                <Divider />
            </Drawer>
        </div>
    );
}