import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import AppBarTemplate from './Template/AppBar';

import { useStyles } from './styles';


export default function App() {

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBarTemplate pageTitle="Gestion de Medicamentos" />
      <main className={classes.content}>
        <div className={classes.toolbar} />

        <Paper className={classes.Paper}>
          <Typography variant="h5" component="h3">
            This is a sheet of paper.
        </Typography>
          <Typography component="p">
            Paper can be used to build surface or other elements for your application.
        </Typography>
        </Paper>

      </main>
    </div>
  );
}