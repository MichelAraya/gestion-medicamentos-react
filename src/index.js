import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom'
// import './index.css';
import App from './App';
import Pacientes from './Modulos/Pacientes/pacientes';
import Medicos from './Modulos/Medicos/medicos';
import Recetas from './Modulos/Recetas/pacientes';

import * as serviceWorker from './serviceWorker';

const routing = (
    <Router>
        <div>
            <Route exact path="/" component={App} />
            <Route exact path="/pacientes" component={Pacientes} />
            <Route exact path="/medicos" component={Medicos} />
            <Route exact path="/recetas" component={Recetas} />
        </div>
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
