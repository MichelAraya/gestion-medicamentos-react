import React from 'react';
import axios from 'axios';

export var Axios = data => {
    return new Promise((resolve, reject) => {
        axios.post(data.url, data)
        .then((response) => {
            resolve(response);
        })
        .catch((error) => {
            reject(error);
        });
    });
}

export class Utils extends React.Component {

    static formatearFecha = (fecha) => {

        var nuevaFecha = new Date(fecha);

        var anio = nuevaFecha.getFullYear();
        var mes = (nuevaFecha.getMonth()+1);
        var dia = nuevaFecha.getDate();

        if (mes < 10) mes = '0' + mes;
        if (dia < 10) dia = '0' + dia;

        var fechaFinal = anio + "-" + mes + "-" + dia;

        return fechaFinal;
    }

    static formatearHora = (hora) => {

        var nuevaFecha = new Date(hora);

        var hora = nuevaFecha.getHours();
        var minutos = nuevaFecha.getMinutes();

        if(hora < 10) {
            hora = "0" + hora;
        }

        if(minutos < 10) {
            minutos = "0" + minutos;
        }

        var fechaFinal = hora + ":" + minutos;

        return fechaFinal;
    }

}