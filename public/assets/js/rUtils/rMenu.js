'use strict';
const rootElementMenu = document.getElementById('menu');

// Create a ES6 class component    
class Menu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            arrayModulos: []
        };

        this.listarModulos();
    }

    listarModulos = () => {
        
        var data = {
            perfil_id: 1
        };

        data.url = process.env.REACT_APP_SERVER+"/modulos_usuarios/por/perfil";
        data.type = "POST";

        Ajax(data).then((respuesta) => {

            this.setState({
                arrayModulos: respuesta
            });
            
        })

    }
    
    // Use the render function to return JSX component      
    render() {

        this.items = this.state.arrayModulos.map((item, key) => {
            return (
                <ul key={'ul_'+key} className="sidebar-menu" data-widget="tree">
                    <li key={'li_'+key}>
                        <a href={item.strModulo}>
                            <i className="fa fa-circle-o"></i> <span>{ item.strTextoModulo }</span>
                        </a>
                    </li>
                </ul>
            )
        });

        return (
            <div>{this.items}</div>
        )
    }
}

function Menu() {
    return(
        <div>
            <Menu/>
        </div>
    )
}

ReactDOM.render(
    <Menu />,
    rootElementMenu
)