function notificar(strText, strType, intTime = 2000) {

    var config;

    if (strType == "warning") {
        config = {
            text: strText,
            position: 'top-center',
            stack: false,
            bgColor: '#D32F2F',
            textColor: 'white',
            icon: 'warning',
            hideAfter: intTime,   // in milli seconds,
            showHideTransition: 'slide'
        };
    } else if (strType == "success") {
        config = {
            text: strText,
            position: 'top-center',
            stack: false,
            bgColor: '#4CAF50',
            textColor: 'white',
            hideAfter: intTime,   // in milli seconds
            showHideTransition: 'slide'
        }
    }

    $.toast(config);

}

// var getModulos = function (data) {
//     return new Promise(function (resolve, reject) {

//         $.ajax({
//             url: "?/cModulos",
//             type: "POST",
//             data: data,
//             success: function (respuesta) {
//                 resolve(respuesta);
//             }
//         });

//     });
// }

// var perfiles = function (data) {
//     return new Promise(function (resolve, reject) {

//         $.ajax({
//             url: "?/cPerfiles",
//             type: "POST",
//             data: data,
//             success: function (respuesta) {
//                 resolve(respuesta);
//             }
//         });

//     });
// }

var Ajax = function (data) {

    return new Promise(function (resolve, reject) {

        $.ajax({
            url: data.url,
            type: data.type,
            data: data,
            success: function (respuesta) {
                resolve(respuesta);
            }
        });

    });

}

// var getBase64 = function getBase64(file) {
//     return new Promise(function (resolve, reject) {
//         var reader = new FileReader();
//         reader.readAsDataURL(file);
//         reader.onload = function () {
//             resolve(reader.result);
//         };
//         reader.onerror = function (error) {
//             reject('Error: ' + error);
//         };
//     });
// }

// $(function () {

//     // Obtenemos URL para controlar en caso de que no exista seteada una opción para el colegio
//     var locationSearch = window.location.search;

//     // Obtenemos el valor del localstorage del rut del colegio
//     var intRutColegio = localStorage.getItem("intRutColegio");

//     // Obtenemos el scope del angular
//     var controllerElement = document.querySelector('body');
//     var scope = angular.element(controllerElement).scope();

//     if (intRutColegio == null) {
//         localStorage.setItem("intRutColegio", "");
//     }

// });

// function getLenguaje() {
//     var lenguaje = {
//         "info": "Hay un total de  _TOTAL_ Transacciones, mostrando (_START_ hasta _END_)",
//         "infoEmpty": "No hay Transacciones disponibles.",
//         "infoFiltered": "- Pagina _PAGE_ de _PAGES_",
//         "loadingRecords": "Cargando...",
//         "processing": "Procesando...",
//         "search": "Buscar : ",
//         "zeroRecords": "No se han encontrado registros.",
//         "emptyTable": "No se han encontrado registros.",
//         "lengthMenu": "Mostrar _MENU_ registros",
//         "paginate": {
//             "first": "Primera",
//             "last": "Ultima",
//             "next": "Siguiente",
//             "previous": "Anterior"
//         }
//     };
//     return lenguaje;
// }


// function validarArchivo(e, input) {
//     // Capturamos el archivo 
//     var file = e.target.files[0].name;

//     // Le sacamos la extensión
//     var extension = file.split('.').pop().toLowerCase();

//     // Definimos un arreglo de opciones válidas 
//     var availableExtensions = ["png","jpg","jpeg"];

//     // Buscamos si la extensión del archivo existe en las opciones válidas
//     var resultado = availableExtensions.filter(ext => ext == extension);

//     // en caso de no haber coincidencias
//     if(resultado.length == 0) {
//         $(input).val("");

//         notificar("Sólo se permite el ingreso de imagenes tipo jpg, jpeg o png","warning", 5000);
//     }
// }